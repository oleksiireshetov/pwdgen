Example of output:


```
#!c#

            var gen = new PasswordGenerator(
                30,
                new SymbolConstraint("0123456789", 2),
                new SymbolConstraint("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", 2),
                new SymbolConstraint("!$%()*+,-:;=? _", 1)
                );
```



```
#!bash

List of 20 generated passwords
130dx:wceCOzPwvOpZFRasI6inPGIQ7
q46ss:evo3vbB4VkFZrPCmECTVRUxXm
f28AK!P4tZEyFWvthCsoyK5ZXSPCVDb
H35Xg(6mkt8ARfwGL5PguEujecjEwhZ
M65CJ_aDLmjZXzBsMbuQUn2189kQyki
F80sm y95CZcDBAnEjxWiqPb2sYxr6M
w30wV=iZwufAdHBYD9DJI2qHSSEm13b
T15Ro:s0XaR8Zu3Nefhdf0utWPIsU5h
I41zx;o4b68CtTinTAhEkjP4YkAOMN1
077Nt?gvY9g137U4t7PLWy52g4lsFyN
T22IV)h6tJrxfAv74jg1HjUp0SomxbT
p84uA=G7wY0zvqU6NkM52ppZDVWpwbH
I33gx)H5L4Js0I0hdHhEX0gKGJM4096
W23EW?H77rKurK1AOZxuueeUlSR985i
f77Iy=F7NNeM7aV2LJGTpHNwlrnlpH0
L40eX%Yo1I9biYZt6ovc2xJBDFnBCkw
Y13Yw;U1JtSfX9XTjm3JAS46Kyaiy0L
G03BF_eu1wVQmyrrpFL1BwoinxI3fFH
q61rz)J7KLCSS4aChB7CaFriDzuT6O3
610ci,MoqkpwYogoXQZGWR09pXkUwcL
*****************************************************
```




```
#!c#

            var gen = new PasswordGenerator(
                20,
                new SymbolConstraint("0123456789", 5),
                new SymbolConstraint("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", 3),
                new SymbolConstraint("!$%()*+,-:;=? _", 6)
                );
```



```
#!bash

List of 20 generated passwords
T76660pMV: ;_:;URPOds
R93855JID $$%,%5M2Q8O
O04281YpB,,$ *:4u3HGP
j66919MXA; ?, %J2ghmE
s85588EUh:=?;+*cDq3k8
N84462aUn%_(=(,HEEf3x
673060fLW_($==-QM5j6U
l01323DJp+%% )-FR4YD4
335603tsn;$:): YJG1YK
x42542rnc:_+:?,iMXYQz
398518iCu:!+*%=ZyCMra
G32609OSl:;:_))ql8pUm
S96675SrR:,,,*!WweRdg
H65081AIu)*;!:;57oeXU
Q28488kyY*;?*+-ahvquJ
H61544QFf:=?=;-sBll4s
v32042GFO_:-%-:yILexw
314080fNh ):?$%pSgMtO
q17015WHf:*$=_?BoIEpv
E28822XWl=_;-=%dM8zNZ
*****************************************************
```