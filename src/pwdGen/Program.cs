﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pwdGen
{
    /// <summary>
    /// Hello. Thank you for your attention. Please note that is solution is limited. In other case to implement it in more correct/extendable way it's 
    /// neccessary to ask question since goals can be different. Unforunatelly I have no time to write unit tests but I believe that you 
    /// just need to understand my way of thinking :)
    /// 
    /// Have a good day :)
    /// </summary>
    public class Program
    {
        const int PasswordCount = 20;
        public void Main(string[] args)
        {
            var gen = new PasswordGenerator(
                30,
                new SymbolConstraint("0123456789", 2),
                new SymbolConstraint("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", 2),
                new SymbolConstraint("!$%()*+,-:;=? _", 1)
                );
            Console.WriteLine(String.Format("List of {0} generated passwords", PasswordCount));
            for (int i = 0; i < PasswordCount; i++)
            {
                Console.WriteLine(gen.Generate(

                ));
            }
            Console.WriteLine("*****************************************************");
            Console.Read();
        }
    }
}
