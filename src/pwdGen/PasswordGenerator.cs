﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Specification
/// Write an application that generates 20 passwords by following rules:

/// * password should be at least 30 symbols in length
/// * password must contain at least 2 numbers from the list 0123456789
/// * password must contain at least 2 characters from the list abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
/// * password must contain at least 1 punctuation mark from the list !$%()*+,-:;=? _
/// * password must begin with a letter
/// </summary>

namespace pwdGen
{

    /// <summary>
    /// Describes a set of symbol constraint abd minimal count of these symbols
    /// e.g. password must contain at least 2 numbers from the list 0123456789 we need 
    /// to create the following object new SymbolConstraint("0123456789", 2) and pass to 
    /// PasswordGenerator ctor();
    /// </summary>
    public class SymbolConstraint
    {
        private String _listOfSymbols;
        private int _minCount;
        private static Random rnd = new Random();

        public int Count
        {
            get { return _minCount; }
            private set { _minCount = value; }
        }

        /// <summary>
        /// Constructor to inizialize object SymbolConstraint type
        /// </summary>
        /// <param name="listOfSymbols">Set of symbols like 0123456789 or  !$%()*+,-:;=? _</param>
        /// <param name="minCount">minimal occurrence in password</param>
        public SymbolConstraint(String listOfSymbols, int minCount)
        {
            if (minCount < 1)
            {
                throw new ArgumentException("minimal number of symbols should be more than zero");
            }
            if (String.IsNullOrEmpty(listOfSymbols))
            {
                throw new ArgumentException("set of symbols cannot be empty");
            }
            _listOfSymbols = listOfSymbols;
            _minCount = minCount;
        }


        /// <summary>
        /// Returns minimal set of symbols
        /// </summary>
        /// <returns></returns>
        public String GetSymbols()
        {
            StringBuilder sb = new StringBuilder();

            for (int i=0;i< _minCount; i++)
            {
                var index = rnd.Next(0, _listOfSymbols.Length);
                sb.Append(_listOfSymbols.ElementAt(index));
            }
            return sb.ToString();
        }
    }


    /// <summary>
    /// This class provides functionality to generate password w/ specified policy rules
    /// </summary>
    public class PasswordGenerator
    {
        private static Random rnd = new Random();
        private int _passwordMinimumLength;
        private int _minLength;
        SymbolConstraint[] _constraintList;
        int _minimumSetOfSymbols;
        /// <summary>
        /// Contructor of PasswordGenerator
        /// </summary>
        /// <param name="minLength">Minimal length of generated password</param>
        /// <param name="constraintList">list of symbol constraints (can be optional parameter)</param>
        public PasswordGenerator(int minLength, params SymbolConstraint[] constraintList)
        {
            _minLength = minLength;
            if (minLength < 1)
            {
                throw new ArgumentException("minimal password length should be more than zero");
            }
            _passwordMinimumLength = minLength;

            if (constraintList != null)
            {
                _constraintList = constraintList;
                int minimumSetOfSymbols = 0;
                foreach (var constraint in constraintList)
                {
                    minimumSetOfSymbols += constraint.Count;
                }
                _minimumSetOfSymbols = minimumSetOfSymbols;
                if (minLength < minimumSetOfSymbols)
                {
                    throw new ArgumentException("minimal password length should be more or equal to total lentgh of all constraint");
                }
            }
        }

                    StringBuilder cSymbols = new StringBuilder();
        public string Generate() {
            StringBuilder cSymbols = new StringBuilder();
            if (_constraintList != null)
            {
                foreach (var constraint in _constraintList)
                {
                    cSymbols.Append(constraint.GetSymbols());
                }
            }
            //We need to generate rest of symbols
            var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var result = new string(
                Enumerable.Repeat(chars, _minLength - _minimumSetOfSymbols)
                          .Select(s => s[rnd.Next(s.Length)])
                          .ToArray());
            //the first symbol should be Letter
            char first;
            do
            {
                first = chars.ElementAt(rnd.Next(chars.Length));
            } while (Char.IsSymbol(first));
            return first + cSymbols.ToString()+result;
        }
    }
}
